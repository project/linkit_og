<?php
/**
 * @file
 * Administration page callbacks for the Linkit OG module.
 */

/**
 * Form callback for the Linkit OG settings.
 */
function linkit_og_admin_settings() {
  $form['linkit_og_tables'] = array(
    '#type' => 'checkboxes',
    '#title' => t("When a user is using Linkit to find these entities, limit results to that user's group memberships"),
    '#default_value' => variable_get('linkit_og_tables', array()),
    '#options' => array(
      'node' => t('Nodes'),
      'taxonomy_term_data' => t('Taxonomy terms'),
      'users' => t('Users'),
      'file_managed' => t('Files'),
    ),
  );
  return system_settings_form($form);
}
