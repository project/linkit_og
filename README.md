# Linkit - Organic Groups

## Overview

This module limits the selection of entities in Linkit to those belonging to the user's groups.

## Installation

1. Enable the module as usual.
2. Visit /admin/config/content/linkit/og to select the entity types you want to limit.
